

//Core Modules
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const logger = require('morgan')
const path = require('path')
const fs = require('fs')
const PORT = process.env.PORT || 3000


//Custom Modules
const app = express()


//Middleware
app.use(bodyParser.json({type:'application/json'}))
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())
app.use(logger('dev'))

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

//test route
app.get('/test', (request, response) => { response.send({message:"connected successfully"})})

//api route for file upload
 app.post('/api/fileupload', (request, response) => {
     //writing uploaded file data to json file
     fs.writeFile('series.json', JSON.stringify({data:request.body.data}), (err) => {  
        // throws an error, you could also catch it here
        if (err){
            console.log(err);
            response.status(500).send({
                status:'false',
                message:'Error in uploading the data to server'
            })
        }
        else {
        console.log('file saved');
        response.status(200).send({
            status:'true',
            message:'successfully uploaded to server'
        })
        }
        
    });
 })

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//creating server
let server = app.listen(PORT, function(req, res) {
    const host = server.address().address;
    const port = server.address().port;
    console.log(`app is listening at ${port}`);
})

module.exports = app

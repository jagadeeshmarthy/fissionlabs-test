import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  title='chart'
  //chart data variables
  static multi = [];
  multi1 = [];
  constructor(private router: Router,private activatedRoute: ActivatedRoute) {
    console.log("=========in chart cons=======")
    console.log(ChartComponent.multi)
    this.multi1 = ChartComponent.multi
   }

  ngOnInit() {
  }
  view: any[] = [700, 400];

  // options for chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Year';
  showYAxisLabel = true;
  yAxisLabel = 'Score';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;
  
  onSelect(event) {
    console.log(event);
  }
}

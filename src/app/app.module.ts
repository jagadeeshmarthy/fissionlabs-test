import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule }   from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { AppComponent } from './app.component';
import { ChartComponent } from './chart/chart.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { fileUploadService } from './services/fileupload.service'


@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    FileuploadComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    NgxChartsModule,
    ToastrModule.forRoot(
      {timeOut: 3000,
        positionClass: 'toast-top-center',}
    ),
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/fileupload',
        pathMatch: 'full'
      },
      {
        path: 'fileupload',
        component: FileuploadComponent
      },
      {
        path: 'chart',
        component: ChartComponent
      }
    ])
  ],
  providers: [fileUploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Injectable} from '@angular/core/';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

const baseAPIUrl = "http://localhost:3000/api";

@Injectable()
export class fileUploadService {
    constructor(private http: Http) {}
    fileUpload(data:any) {
        console.log("in service")
        return this.http.post(baseAPIUrl + '/fileupload', {data:data});
    };
}
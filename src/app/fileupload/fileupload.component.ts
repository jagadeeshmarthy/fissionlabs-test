import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ChartComponent} from '../chart/chart.component'
import { fileUploadService} from '../services/fileupload.service'

@Component({
  selector: 'app-fileupload', 
  templateUrl: './fileupload.component.html',
   styleUrls: ['./fileupload.component.css']
  })
export class FileuploadComponent implements OnInit {
  title = 'fileupload';
  files : FileList;
  chartData : any;
  constructor(private toastr : ToastrService, private router : Router, private fileUploadService: fileUploadService) {}
  ngOnInit() {}

  //file upload listener 
  fileUploadListener(event) {
    let finalData = [];
    let multi = [];
    this.files = event.target.files; // FileList object
    let file = this.files[0];
    console.log(file)
    let reader : any;
    reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function (event) {
      var csv = event.target.result;
      console.log(csv)
      let csvData = csv;
      let allTextLines = csvData.split(/\r\n|\n/);
      for (let i = 0; i < allTextLines.length; i++) {
        let headers = allTextLines[i].split(',');
        let series = { name: '',series: []}
        for (let j = 1; j < headers.length; j++) {
          let seriesData = { name: '', value: 0 };
          series.name = headers[0];
          seriesData.name = headers[j].split("|")[0];
          seriesData.value = headers[j].split("|")[1];
          series.series.push(seriesData)
        }
        multi.push(series)
      }
      ChartComponent.multi = multi;
    }
  }
  //redirection on submitting
  route() {
    console.log("========in router========")
    console.log(this.files[0].name);
    console.log(ChartComponent.multi)
    this.fileUploadService.fileUpload(ChartComponent.multi).map(res => res.json()).subscribe(data => {
        this.toastr.success(data.message);
        this.router.navigate(['/chart']);
      },
    error => {
      console.log("in data uploading error")
      this.toastr.error(JSON.parse(error._body).message);
    })
  }
}
